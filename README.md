# Attention!
This project is under development.
You can not play that game until
beta is realeased. Database also not included.

# How to Install
To make sure that all required modules installed,
please run:
```sh
$ npm i
```
# How to start project
You can start project with command:
```sh
$ npm run dev
```
Also server and client side is seperated,
so you can run it with:

Client side
```sh
$ npm run client
```
Server side
```sh
$ npm run server
```
