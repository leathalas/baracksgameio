class ConfigHelper {
  constructor() {
    this.modulesConfigs = this.importAllModulesConfigs();
  }

  importAllModulesConfigs() {
    const modules = require.context('../../../', true, /app\.config\.js$/);
    return modules.keys().map(modules);
  }

  getStores() {
    return this.getOptionsByKey('stores');
  }

  getRoutes() {
    const configRoutes = this.getOptionsByKey('routes');
    const routes = [];
    Object.values(configRoutes).forEach(route => {
      routes.push(route);
    });
    return routes;
  }

  getOptionsByKey(key) {
    const options = {};
    [...this.modulesConfigs].forEach(config => {
      if (config && config[key]) {
        Object.assign(options, config[key]);
      }
    });
    return options;
  }
}

export default new ConfigHelper();
