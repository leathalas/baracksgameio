import { runInAction } from 'mobx';
import axios from 'axios';

class UsersStore {
  constructor() {
    this.userSignIn = this.userSignIn.bind(this);
    this.userSignUp = this.userSignUp.bind(this);
  }

  userSignIn(username, password) {
    return new Promise((resolve, reject) => {
      axios
        .post('/api/login', { username, password })
        .then(response => {
          if (response && response.data) {
            const { data = {} } = response;
            runInAction(() => {
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Login failed'));
              }
            });
          }
        });
    });
  }

  userSignUp(username, email, password) {
    return new Promise((resolve, reject) => {
      axios
        .post('/api/register', { username, email, password })
        .then(response => {
          if (response && response.data) {
            const { data = {} } = response;
            runInAction(() => {
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Login failed'));
              }
            });
          }
        });
    });
  }
}

export default new UsersStore();