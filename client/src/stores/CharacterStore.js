import axios from 'axios';
import { observable, decorate, action, runInAction } from 'mobx';

const pointsStats = {
  atk: 0,
  def: 0,
  vit: 0,
  agi: 0,
};

const armourStats = {
  atk: 0,
  def: 0,
  vit: 0,
  agi: 0,
};

const playerInfo = {
  playerPoints: 0,
  playerLevel: 0,
  playerXp: 0,
  playerLvlUpXp: 0,
  playerCurrXp: 0,
  playerHealth: 0,
};

class CharacterStore {
  constructor() {
    this.pointsStats = pointsStats;
    this.armourStats = armourStats;
    this.playerInfo = playerInfo;
    this.playerPotions = [];
  }

  loadPotionStorage(playerid) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/player/potions/all/${playerid}`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                this.playerPotions = data.data;
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  loadPoints(playerId) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/player/info/points/${playerId}`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                // User Points
                this.pointsStats.atk = data.data.p_points_atk;
                this.pointsStats.def = data.data.p_points_def;
                this.pointsStats.vit = data.data.p_points_vit;
                this.pointsStats.agi = data.data.p_points_agi;
                // User armour stats
                this.armourStats.atk = data.data.e_points_atk;
                this.armourStats.def = data.data.e_points_def;
                this.armourStats.vit = data.data.e_points_vit;
                this.armourStats.agi = data.data.e_points_agi;
                // User stats
                this.playerInfo.playerPoints = data.data.player_points;
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  loadBase(playerId) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/player/info/base/${playerId}`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                this.playerInfo.playerLevel = data.data.player_level;
                this.playerInfo.playerXp = data.data.player_xp;
                this.playerInfo.playerLvlUpXp = data.data.player_lvlup_xp;
                this.playerInfo.playerCurrXpp = data.data.player_curr_xp_p;
                this.playerInfo.playerHealth = data.data.player_health;
                this.playerInfo.playerHealtHp = data.data.player_health_p;
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  handleStatAdd(statname, playerId) {
    const opts = {
      playerid: playerId,
      statname: statname,
    };
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/player/add/player/stats/`, opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  usePotion(potionid, playerId) {
    const opts = {
      potionid: potionid,
      playerid: playerId,
    };
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/player/potions/use/`, opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }
}

decorate(CharacterStore, {
  playerPotions: observable,
  pointsStats: observable,
  armourStats: observable,
  playerInfo: observable,
  loadPotionStorage: action,
  loadPoints: action,
  loadBase: action,
  handleStatAdd: action,
  usePotion: action,
});

export default new CharacterStore();