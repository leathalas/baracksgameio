import axios from 'axios';
import { observable, decorate, action, runInAction } from 'mobx';

class MobsStore {
  constructor() {
    this.mobs = [];
  }

  loadMobs() {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/mobarena/mobs/all`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                this.mobs = data.data.sort((a, b) => {
                  return a.mob_difficulty - b.mob_difficulty;
                });
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  handleMobFight(playerId, mobId) {
    const opts = {
      playerid: playerId,
      mobid: mobId,
    };
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/mobarena/fight/mob/`, opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  };
}

decorate(MobsStore, {
  mobs: observable,
  loadMobs: action,
});

export default new MobsStore();