import axios from 'axios';
import { observable, decorate, action, runInAction } from 'mobx';

class InventoryStore {
  constructor() {
    this.equipment = [];
    this.inventory = [];
    this.items = [];
  }

  loadPlayerInventory(userId) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/player/inventory/${userId}`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                this.inventory = data.data;
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  loadPlayerEquipment(userId) {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/player/equipment/${userId}`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                this.equipment = data.data;
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  loadAllItems() {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/item/action/find/find_all/null`)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                this.items = data.data;
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  }

  equipRequest(itemId, userId) {
    const opts = {
      itemid: itemId,
      playerid: userId,
    };
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/equipment/equipitem/`, opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  };

  unEquipRequest(itemId, userId, itemType) {
    const opts = {
      itemid: itemId,
      playerid: userId,
      itemtype: itemType,
    };
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/equipment/unequipitem/`, opts)
        .then(response => {
          if (response && response.data) {
            runInAction(() => {
              const { data } = response;
              if (data) {
                resolve(data);
              } else {
                reject(new Error('Request error'));
              }
            });
          }
        });
    });
  };
}

decorate(InventoryStore, {
  items: observable,
  equipment: observable,
  inventory: observable,
  loadAllItems: action,
  loadPlayerInventory: action,
  loadPlayerEquipment: action,
});

export default new InventoryStore();