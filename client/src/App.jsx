import React from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PropTypes from 'prop-types';
import TopNav from "./components/partials/topNav";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.getRoutes = this.getRoutes.bind(this);
  }

  getRoutes() {
    const { routes } = this.props;
    const appRoutes = [];
    routes.forEach((route, i) => {
      appRoutes.push(<Route key={i} {...route} />);
    });
    return appRoutes;
  }

  render() {
    return (
      <Router>
        <TopNav />
        <Switch>{this.getRoutes()}</Switch>
      </Router>
    );
  }
}

App.propTypes = {
  location: PropTypes.object,
  routes: PropTypes.array.isRequired,
};

export default App;
