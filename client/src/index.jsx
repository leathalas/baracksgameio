import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'mobx-react-router';
import ConfigHelper from './helpers/ConfigHelper';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications-component/dist/theme.css';
import './index.css';


const stores = ConfigHelper.getStores();
const routes = ConfigHelper.getRoutes();

const browserHistory = createBrowserHistory();
const appHistory = syncHistoryWithStore(browserHistory, stores.routerStore);

ReactDOM.render(
  <Provider {...stores}>
    <Router history={appHistory}>
      <App routes={routes} />
    </Router>
  </Provider>,
  document.getElementById('root')
);
