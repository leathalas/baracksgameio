import React, { Component } from "react";
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';

import {
  Table,
  Row,
  Col,
  ProgressBar,
  Button,
  Container,
  Alert,
} from 'react-bootstrap';

class Character extends Component {
  constructor(props) {
    super(props);
    this.characterStore = this.props.characterStore;
    this.playerId = window.sessionStorage.getItem("id");
    this.handleStatAdd = this.handleStatAdd.bind(this);
    this.usePotion = this.usePotion.bind(this);
  }

  componentDidMount() {
    this.updatePotionStorage();
    this.fetchAndSetStats();
  }

  updatePotionStorage() {
    const { characterStore } = this.props;
    characterStore.loadPotionStorage(this.playerId);
  }

  fetchAndSetStats() {
    const { characterStore } = this.props;
    characterStore.loadPoints(this.playerId);
    characterStore.loadBase(this.playerId);
  }

  handleStatAdd(statName) {
    const { characterStore } = this.props;
    characterStore.handleStatAdd(statName, this.playerId);
    this.fetchAndSetStats();
  }

  usePotion(potionId) {
    const { characterStore } = this.props;
    characterStore.usePotion(potionId, this.playerId).then(
      this.fetchAndSetStats(),
      this.updatePotionStorage(),
    );
  }

  renderPotions() {
    const { characterStore } = this.props;
    const { playerPotions } = characterStore;
    const items = [];

    playerPotions.map((potion, index) => {
      items.push(
        <tr key={index}>
          <th>{potion.potion_name}</th>
          <td>{potion.type}</td>
          <td>{potion.addition}</td>
          <td>
            <Button
              onClick={this.usePotion.bind(this, potion._id)}
              className="btn btn-primary"
            >
              Use
            </Button>
          </td>
        </tr>
      );
    });
    return items;
  }

  render() {
    const { pointsStats, armourStats, playerInfo, playerPotions } = this.characterStore;
    return (
      <div>
        <Alert variant="dark">
          <Row>
            <Col>Current level: {playerInfo.playerLevel}</Col>
            <Col>Current xp: {playerInfo.playerXp} / {playerInfo.playerLvlUpXp}</Col>
            <Col>
              <ProgressBar
                now={playerInfo.playerCurrXpp}
                min={0}
                max={100}
                style={{ height: 25 }}
              />
            </Col>
            <Col>Current Health: {playerInfo.playerHealth}</Col>
            <Col>
              <ProgressBar
                variant="danger"
                now={playerInfo.playerHealth}
                min={0}
                max={100}
                style={{ height: 25 }}
              />
            </Col>
          </Row>
        </Alert>
        <Container className="mt-2">
          <h1 className="text-center"> Stats </h1>
          <Alert variant="dark">
            Points left: {playerInfo.playerPoints}
          </Alert>
          <Row>
            <Col className="text-center">
              <Table striped bordered>
                <thead className="table-dark">
                  <tr>
                    <th>Stat name</th>
                    <th>Current points</th>
                    <th>Add points</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>Attack</th>
                    <td> +{pointsStats.atk} </td>
                    <td>
                      <Button onClick={this.handleStatAdd.bind(this, "atk")}>+</Button>
                    </td>
                  </tr>
                  <tr>
                    <th>Deffence</th>
                    <td> +{pointsStats.def} </td>
                    <td>
                      <Button onClick={this.handleStatAdd.bind(this, "def")}>+</Button>
                    </td>
                  </tr>
                  <tr>
                    <th>Vitality</th>
                    <td> +{pointsStats.vit} </td>
                    <td>
                      <Button onClick={this.handleStatAdd.bind(this, "vit")}>+</Button>
                    </td>
                  </tr>
                  <tr>
                    <th>Agility</th>
                    <td> +{pointsStats.agi} </td>
                    <td>
                      <Button onClick={this.handleStatAdd.bind(this, "agi")}>+</Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col className="text-center">
              <Table striped bordered>
                <thead className="table-dark">
                  <tr>
                    <th>Stat name</th>
                    <th>Armors addition</th>
                    <th>Total points</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>Attack</th>
                    <td> +{armourStats.atk} </td>
                    <td> +{pointsStats.atk + armourStats.atk} </td>
                    <td>
                      <Button variant="warning"> Review </Button>
                    </td>
                  </tr>
                  <tr>
                    <th>Deffence</th>
                    <td> +{armourStats.def} </td>
                    <td> +{pointsStats.def + armourStats.def} </td>
                    <td>
                      <Button variant="warning"> Review </Button>
                    </td>
                  </tr>
                  <tr>
                    <th>Vitality</th>
                    <td> +{armourStats.vit} </td>
                    <td> +{pointsStats.vit + armourStats.vit} </td>
                    <td>
                      <Button variant="warning"> Review </Button>
                    </td>
                  </tr>
                  <tr>
                    <th>Agility</th>
                    <td> +{armourStats.agi} </td>
                    <td> +{pointsStats.agi + armourStats.agi} </td>
                    <td>
                      <Button variant="warning"> Review </Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Col>
          </Row>
          <Table striped bordered className="text-center">
            <thead className="table-dark">
              <tr>
                <th>Potion Name</th>
                <th>Potion Type</th>
                <th>Potion Addition</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {playerPotions && this.renderPotions()}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

Character.propTypes = {
  characterStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('characterStore'), observer);

export default enhance(Character);
