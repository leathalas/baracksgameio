import React, { Component } from "react";
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';

import {
  Table,
  Row,
  Col,
  ProgressBar,
  Button,
  Container,
  Alert,
} from 'react-bootstrap';

class Inventory extends Component {
  constructor(props) {
    super(props);
    this.characterStore = this.props.characterStore;
    this.inventoryStore = this.props.inventoryStore;
    this.itemSendUnEquipRequest = this.itemSendUnEquipRequest.bind(this);
    this.itemSendEquipRequest = this.itemSendEquipRequest.bind(this);
    this.userId = window.sessionStorage.getItem('id');
  }

  componentDidMount() {
    const { characterStore, inventoryStore } = this.props;
    characterStore.loadBase(this.userId);
    this.getPlayerInventory();
    inventoryStore.loadAllItems();
  }

  getPlayerInventory() {
    const { inventoryStore } = this.props;
    inventoryStore.loadPlayerInventory(this.userId);
    inventoryStore.loadPlayerEquipment(this.userId);
  }

  getItemStatsByPref(id, pref) {
    const { items } = this.inventoryStore;
    var itemStats = `item_${pref}`;
    return items.filter(items => items._id === id).map((item, index) => (
      <td key={[item._id + index]}>{item[itemStats]}</td>
    ));
  }

  itemSendEquipRequest(itemId) {
    const { inventoryStore } = this.props;
    inventoryStore.equipRequest(itemId, this.userId);
    this.getPlayerInventory();
  };

  itemSendUnEquipRequest(itemId, itemType) {
    const { inventoryStore } = this.props;
    inventoryStore.unEquipRequest(itemId, this.userId, itemType);
    this.getPlayerInventory();
  };

  render() {
    const { inventory, equipment } = this.inventoryStore;
    const { playerInfo } = this.characterStore;
    return (
      <div>
        <Alert variant="dark">
          <Row>
            <Col>Current level: {playerInfo.playerLevel}</Col>
            <Col>Current xp: {playerInfo.playerXp} / {playerInfo.playerLvlUpXp}</Col>
            <Col>
              <ProgressBar
                now={playerInfo.playerCurrXpp}
                min={0}
                max={100}
                style={{ height: 25 }}
              />
            </Col>
            <Col>Current Health: {playerInfo.playerHealth}</Col>
            <Col>
              <ProgressBar
                variant="danger"
                now={playerInfo.playerHealth}
                min={0}
                max={100}
                style={{ height: 25 }}
              />
            </Col>
          </Row>
        </Alert>
        <Container className="mt-2">
          <h1 className="text-center">Current Equipement</h1>
          <Table striped bordered className="text-center">
            <thead className="table-dark">
              <tr>
                <td>#</td>
                <td>Item</td>
                <td>Type</td>
                <td>+DEF</td>
                <td>+AGI</td>
                <td>+VIT</td>
                <td>+ATK</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {equipment.length > 0 && equipment.map((item, index) => (
                <tr key={index}>
                  <td>{index}</td>
                  <td>{item.item_name}</td>
                  <td>{item.item_type}</td>
                  {this.getItemStatsByPref(item.item_id, "def")}
                  {this.getItemStatsByPref(item.item_id, "agi")}
                  {this.getItemStatsByPref(item.item_id, "vit")}
                  {this.getItemStatsByPref(item.item_id, "atk")}
                  <td>
                    <Button
                      variant="danger"
                      onClick={this.itemSendUnEquipRequest.bind(this, item.item_id, item.item_type)}
                    >
                      Unequip
                    </Button>
                  </td>
                </tr>
              ))}
              <tr className="bg-dark text-light">
                <td>#</td>
                <td></td>
                <td>Total:</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </Table>
          <h1 className="text-center">Inventory</h1>
          <Table striped bordered className="text-center">
            <thead className="table-dark">
              <tr>
                <td>#</td>
                <td>Item</td>
                <td>Type</td>
                <td>+DEF</td>
                <td>+AGI</td>
                <td>+VIT</td>
                <td>+ATK</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              {inventory.length > 0 && inventory.map((item, index) => (
                <tr key={index}>
                  <td>{index}</td>
                  <td>{item.item_name}</td>
                  <td>{item.item_type}</td>
                  {this.getItemStatsByPref(item.item_id, "def")}
                  {this.getItemStatsByPref(item.item_id, "agi")}
                  {this.getItemStatsByPref(item.item_id, "vit")}
                  {this.getItemStatsByPref(item.item_id, "atk")}
                  <td>
                    <Button
                      className="btn btn-success"
                      onClick={this.itemSendEquipRequest.bind(this, item.item_id)}
                    >
                      Equip
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

Inventory.propTypes = {
  characterStore: PropTypes.object.isRequired,
  inventoryStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('characterStore', 'inventoryStore'), observer);

export default enhance(Inventory);
