import React, { Component } from 'react';

class logout extends Component {
  componentDidMount() {
    window.sessionStorage.setItem("x-auth-token", "");
    window.location.href = '/';
  }

  render() {
    return (
      <div></div>
    );
  }
}

export default logout;