import React, { Component } from "react";
import { compose } from 'recompose';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';

import {
  Button,
  Row,
  Col,
  ProgressBar,
  ListGroup,
  Card,
  Container,
  Alert,
} from 'react-bootstrap';

import ReactNotification from 'react-notifications-component';
import { store } from 'react-notifications-component';

class MobArena extends Component {
  constructor(props) {
    super(props);
    this.characterStore = this.props.characterStore;
    this.mobsStore = this.props.mobsStore;
    this.userId = window.sessionStorage.getItem('id');
  }

  componentDidMount() {
    const { mobsStore } = this.props;
    mobsStore.loadMobs();
    this.refreshProgress();
  }

  refreshProgress() {
    const { characterStore } = this.props;
    characterStore.loadBase(this.userId);
  }

  handleMobFight(mobId) {
    const { mobsStore } = this.props;
    mobsStore.handleMobFight(this.userId, mobId).then(data => {
      if (data) {
        if (!data.data.isPlayerWin) {
          store.addNotification({
            title: "Sorry!",
            message: "Try your luck with a better gear :/",
            type: "danger",
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 1000,
              onScreen: true,
            },
          });
        }
        if (data.drop) {
          store.addNotification({
            title: "Congratulations!",
            message: `Lucky day today! new item in your inventory -> ${data.drop.item_name}!`,
            type: "info",
            insert: "top",
            container: "top-right",
            animationIn: ["animated", "fadeIn"],
            animationOut: ["animated", "fadeOut"],
            dismiss: {
              duration: 5000,
              onScreen: true,
            },
          });
        }
      }
    });
    this.refreshProgress();
    return null;
  };

  render() {
    const { mobs } = this.mobsStore;
    const { playerInfo } = this.characterStore;
    return (
      <div>
        <ReactNotification />
        <Alert variant="dark">
          <Row>
            <Col>Current level: {playerInfo.playerLevel}</Col>
            <Col>Current xp: {playerInfo.playerXp} / {playerInfo.playerLvlUpXp}</Col>
            <Col>
              <ProgressBar
                now={playerInfo.playerCurrXpp}
                min={0}
                max={100}
                style={{ height: 25 }}
              />
            </Col>
            <Col>Current Health: {playerInfo.playerHealth}</Col>
            <Col>
              <ProgressBar
                variant="danger"
                now={playerInfo.playerHealth}
                min={0}
                max={100}
                style={{ height: 25 }}
              />
            </Col>
          </Row>
        </Alert>
        <Container className="mt-2">
          <Row>
            {mobs && mobs.map((mob, index) =>
              <Col key={index}>
                <Card>
                  <Card.Body>
                    <Card.Title>{mob.mob_name}</Card.Title>
                  </Card.Body>
                  <ListGroup variant="flush">
                    <ListGroup.Item>Attack: {mob.mb_stats_atk}</ListGroup.Item>
                    <ListGroup.Item>Deffence: {mob.mb_stats_def}</ListGroup.Item>
                    <ListGroup.Item>Agility: {mob.mb_stats_agi}</ListGroup.Item>
                    <ListGroup.Item>Health: {mob.mb_stats_vit}</ListGroup.Item>
                  </ListGroup>
                  <Card.Body>
                    <Button onClick={this.handleMobFight.bind(this, mob._id)}>Fight</Button>
                  </Card.Body>
                </Card>
              </Col>
            )}
          </Row>
        </Container>
      </div>
    );
  }
}

MobArena.propTypes = {
  characterStore: PropTypes.object.isRequired,
  mobsStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('characterStore', 'mobsStore'), observer);

export default enhance(MobArena);