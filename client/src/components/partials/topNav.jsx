import React, { Component } from "react";
import { inject, observer } from 'mobx-react';
import { compose } from 'recompose';
import decode from 'jwt-decode';
import { Navbar, Container, Nav } from 'react-bootstrap';
import PropTypes from 'prop-types';

class TopNav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      values: {
        id: "",
        username: "",
        email: "",
      },
      token: ""
    };
  }

  componentDidMount() {
    this.setState({ token: this.getToken() }, () => {
      if (this.state.token) {
        const decoded = decode(this.state.token);
        this.setState({
          id: decoded.user._id,
          username: decoded.user.username,
          email: decoded.user.email
        });
        window.sessionStorage.setItem("id", decoded.user._id);
        window.sessionStorage.setItem("email", decoded.user.email);
        window.sessionStorage.setItem("username", decoded.user.username);
      }
    });
  }

  getToken = () => {
    const token = window.sessionStorage.getItem("x-auth-token");
    if (token) return token;
    else return "";
  };

  render() {
    let navLeft, navRight;
    if (!this.state.token) {
      navLeft = [
        <Nav className="mr-auto" key={'auth'}>
          <Nav.Link href="/sign-in">Sign In</Nav.Link>
          <Nav.Link href="/sign-up">Sign Up</Nav.Link>
        </Nav>
      ];
    } else {
      navLeft = [
        <Nav key={'actions'}>
          <Nav.Link href="/log-out">Log out</Nav.Link>
        </Nav>
      ];
      navRight = [
        <Nav className="mr-auto" key={'arena'}>
          <Nav.Link href="/character">Character</Nav.Link>
          <Nav.Link href="/inventory">Inventory</Nav.Link>
          <Nav.Link href="/mobarena">Mob Arena</Nav.Link>
        </Nav>
      ];
    }

    return (
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/">Baracks.io</Navbar.Brand>
          {navRight}
          <Nav>
            {navLeft}
          </Nav>
        </Container>
      </Navbar>
    );
  }
}

TopNav.propTypes = {
  usersStore: PropTypes.object,
};

const enhance = compose(inject('usersStore'), observer);

export default enhance(TopNav);