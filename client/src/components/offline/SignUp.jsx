import React, { Component } from "react";
import { Button, Form } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { inject, observer } from 'mobx-react';

import ReactNotification from 'react-notifications-component';
import { store } from 'react-notifications-component';

class SignUp extends Component {
  constructor(props) {
    super(props);
    const formValues = {
      username: '',
      password: '',
      email: '',
    };
    this.state = {
      formValues: formValues,
      isSubmitting: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  componentDidMount() {
    if (window.sessionStorage.getItem("x-auth-token")) {
      window.location.href = "/";
    }
  }

  handleSubmit() {
    const { username, password, email } = this.state.formValues;
    const { usersStore } = this.props;
    this.setState({ isSubmitting: true });

    usersStore.userSignUp(username, email, password).then(data => {
      if (!data.errors) {
        window.location.href = "/";
      } else {
        store.addNotification({
          title: "Error:",
          message: data.errors,
          type: "danger",
          insert: "top",
          container: "top-right",
          animationIn: ["animated", "fadeIn"],
          animationOut: ["animated", "fadeOut"],
          dismiss: {
            duration: 5000,
            onScreen: true,
          },
        });
      }
      this.setState({
        formValues: { username: "", password: "" },
      });
    });
  };

  handleInputChange(event) {
    this.setState({
      formValues: { ...this.state.formValues, [event.target.name]: event.target.value }
    });
  };

  render() {
    return (
      <div className="auth-wrapper">
        <ReactNotification />
        <div className="auth-inner">
          <Form>
            <h3>Sign Up</h3>
            <Form.Group controlId="username">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                name="username"
                placeholder="Enter username"
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="email">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                name="email"
                placeholder="Enter email"
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                name="password"
                placeholder="Enter password"
                onChange={this.handleInputChange}
              />
            </Form.Group>
            <Button onClick={this.handleSubmit} variant="primary">Submit</Button>
            <p className="forgot-password text-right">
              Already registered <a href="/sign-in">sign in?</a>
            </p>
          </Form>
        </div>
      </div>
    );
  }
}

SignUp.propTypes = {
  usersStore: PropTypes.object.isRequired,
};

const enhance = compose(inject('usersStore'), observer);

export default enhance(SignUp);
