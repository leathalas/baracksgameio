import UsersStore from './stores/UsersStore';
import RouterStore from './stores/RouterStore';
import CharacterStore from './stores/CharacterStore';
import InventoryStore from './stores/InventoryStore';
import MobsStore from './stores/MobsStore';
import SignIn from './components/offline/SignIn';
import SignUp from './components/offline/SignUp';
import logout from './components/online/logout';
import Character from './components/online/Character';
import Inventory from './components/online/Inventory';
import MobArena from './components/online/mobarena';

const enabled = true;

const stores = {
  usersStore: UsersStore,
  routerStore: RouterStore,
  characterStore: CharacterStore,
  inventoryStore: InventoryStore,
  mobsStore: MobsStore,
};

const routes = [
  {
    path: '/sign-in',
    component: SignIn,
  },
  {
    path: '/sign-up',
    component: SignUp,
  },
  {
    path: '/log-out',
    component: logout,
  },
  {
    path: '/inventory',
    component: Inventory,
  },
  {
    path: '/character',
    component: Character,
  },
  {
    path: '/mobarena',
    component: MobArena,
  }
];

export { enabled, routes, stores };
