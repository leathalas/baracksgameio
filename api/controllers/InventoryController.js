const InventoryService = require('../services/InventoryService');

module.exports = {
  getPlayerInventory: async (req, res, next) => {
    let params = {
      playerId: req.params.playerid
    }
    const data = await InventoryService.getInventory(params);
    res.json(data);
  },
  additemToInventory: async (req, res, next) => {
    let params = {
      item_name: "Golden Chestplate",
      item_type: "Chestplate",
      item_id: "5e93a6bd1edf3c6e6b47c3f4",
      owner_id: "5eb066416beb1d24b3a437c8",
    };
    const data = await InventoryService.addItem(params);
    res.json(data);
  }
};