const ProgressService = require("../services/ProgressService");

module.exports = {
  getPlayerProgress: async (req, res) => {
    let params = {
      playerId: req.params.playerid,
    };
    const data = await ProgressService.getPlayerProgress(params);
    res.json(data);
  }
}