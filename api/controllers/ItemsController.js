const ItemsService = require('../services/ItemsService');

module.exports = {
  itemsActionFind: async (req, res, next) => {
    let params = {
      searchType: req.params.searchtype,
      searchBody: req.params.searchbody,
    };
    const data = await ItemsService.itemsActionFind(params);
    res.json(data);
  },
};