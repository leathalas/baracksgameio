const EquipmentService = require('../services/EquipmentService');

module.exports = {
  equipItemById: async (req, res, next) => {
    let params = {
      itemId: req.body.itemid,
      playerId: req.body.playerid,
    };
    const data = await EquipmentService.equipItem(params);
    res.json(data);
  },
  unequipItemById: async (req, res, next) => {
    let params = {
      itemType: req.body.itemtype,
      playerId: req.body.playerid,
    };
    const data = await EquipmentService.unequipItem(params);
    res.json(data);
  },
  getPlayerEquipment: async (req, res, next) => {
    let params = {
      playerId: req.params.playerid,
    };
    const data = await EquipmentService.getEquipment(params);
    res.json(data);
  }
};