const PotionsService = require('../services/PotionsService');

module.exports = {
  usePotion: async (req, res) => {
    let params = {
      playerId: req.body.playerid,
      potionId: req.body.potionid,
    };
    const data = await PotionsService.usePotion(params);
    res.json(data);
  },
  potionsStorage: async (req, res) => {
    let params = {
      playerId: req.params.playerid,
    };
    const data = await PotionsService.findPotions(params);
    res.json(data);
  }
};