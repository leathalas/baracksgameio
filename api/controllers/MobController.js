const MobService = require("../services/MobService");

module.exports = {
  getAllMobs: async (req, res) => {
    const data = await MobService.getAllMobs();
    res.json(data)
  },
  fightMob: async (req, res) => {
    let params = {
      playerId: req.body.playerid,
      mobId: req.body.mobid,
    }
    const data = await MobService.fightMob(params);
    res.json(data);
  },
  createMob: async (req, res) => {
    let params = {
      mob_name: "Dog",
      mob_difficulty: 5,
      mb_stats_def: 50,
      mb_stats_atk: 64,
      mb_stats_agi: 12,
      mb_stats_vit: 360,
    };
    const data = await MobService.addNewMob(params);
    res.json(data);
  }
};