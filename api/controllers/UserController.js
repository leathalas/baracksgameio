const UserService = require('../services/UserService');
const { validationResult } = require('express-validator');

module.exports = {
  login: async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array()[0].msg });
    }

    let params = {
      username: req.body.username,
      password: req.body.password,
    };

    const data = await UserService.connectUser(params);
    res.json(data);
  },
  register: async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array()[0].msg });
    }

    let params = {
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
    };

    const data = await UserService.createUser(params);
    res.json(data);
  }
};