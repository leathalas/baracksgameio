const PointsService = require("../services/PointsService");

module.exports = {
  getPlayerPointsInfo: async (req, res) => {
    //TODO: TOKEN implementation
    let params = {
      playerId: req.params.playerid
    }
    const data = await PointsService.getPlayerStats(params);
    res.json(data);
  },
  addPlayerStats: async (req, res) => {
    //TODO: TOKEN implementation
    let params = {
      playerId: req.body.playerid,
      statName: req.body.statname
    }
    const data = await PointsService.addPlayerPtStats(params);
    res.json(data);
  }
}