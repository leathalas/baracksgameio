const {body} = require('express-validator');

exports.validate = function (method) {
    'use strict';
    switch (method) {
    case 'UserRegister':
        return [
            body('email', 'Invalid email form please verify').exists().isEmail(),
            body('username', 'Username length: 6-50 please choose another').isLength({min: 6, max: 50}),
            body('password', 'Password length: 8-50 please choose another').isLength({min: 8, max: 50})
        ];
    }
};