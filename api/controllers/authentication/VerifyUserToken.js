
module.exports = function verifyToken(req, res, next) {
    'use strict';
    const auth = req.body.auth;
    if (auth !== 'undefined') {
        req.token = auth;
        next();
    }
};