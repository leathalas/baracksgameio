var PointsService = { 
  getPlayerStats: function (params) {
    return getPlayerStats(params);
  },
  addPlayerPtStats: function (params) {
    return addPlayerPtStats(params);
  },
  updatePlayerEqStats: function (playerId) {
    return updatePlayerEqStats(playerId);
  },
};

module.exports = PointsService;

const User = require("../models/UserModel").User;
const Item = require("../models/ItemsModel").Item;

const EquipmentService = require('../services/EquipmentService');

const mongoose = require("mongoose");

async function getPlayerStats (params) {
  var result = null;
  if (!mongoose.isValidObjectId(params.playerId)) {
      return { data: result, errors: "Cannot find user" };
  }
  const user = await User.findOne({ _id: params.playerId });
  if (!user) {
      return { data: result, errors: "Cannot find user" };
  }
  result = {
    p_points_def: user.pt_stats_def,
    p_points_agi: user.pt_stats_agi,
    p_points_vit: user.pt_stats_vit,
    p_points_atk: user.pt_stats_atk,
    e_points_def: user.eq_stats_def,
    e_points_agi: user.eq_stats_agi,
    e_points_vit: user.eq_stats_vit,
    e_points_atk: user.eq_stats_atk,
    player_points: user.player_points,
  };
  return { data: result, errors: "" };
}

async function addPlayerPtStats (params) {
  var newData = null;
  if (!mongoose.isValidObjectId(params.playerId)) {
    return { data: newData, errors: "Cannot find user" };
  }

  const user = await User.findOne({ _id: params.playerId });

  if (!user) {
    return { data: newData, errors: "Cannot find user" };
  }

  newData = {
    points_def: user.pt_stats_def,
    points_agi: user.pt_stats_agi,
    points_vit: user.pt_stats_vit,
    points_atk: user.pt_stats_atk,
    player_points: user.player_points,
  };

  if (user.player_points <= 0) {
    return { data: null, errors: "Invalid action" };
  }

  switch (params.statName) {
    case "def": {
      newData.points_def += 1;
      newData.player_points -=1;
      break;
    }
    case "agi": {
      newData.points_agi += 1;
      newData.player_points -=1;
      break;
    }
    case "vit": {
      newData.points_vit += 1;
      newData.player_points -= 1;
      break;
    }
    case "atk": {
      newData.points_atk += 1;
      newData.player_points -=1;
      break;
    }
    default: {
      return { data: null, errors: "Invalid action" };
    }
  };

  await User.updateMany({ _id: params.playerId }, { 
    pt_stats_def: newData.points_def,
    pt_stats_agi: newData.points_agi,
    pt_stats_vit: newData.points_vit,
    pt_stats_atk: newData.points_atk,
    player_points: newData.player_points,
  });

  return { data: newData, errors: "" };
}

async function updatePlayerEqStats(playerId) {
  let params = {
    playerId: playerId,
  };

  const equipment = await EquipmentService.getEquipment(params);
  
  var newEq = { def: 0, vit: 0, agi: 0, atk: 0 };

  for (var i = 0; i < equipment.data.length; i++ ) {
    await Item.findById(equipment.data[i].item_id).then((find) => {
      newEq.def += find.item_def;
      newEq.vit += find.item_vit;
      newEq.agi += find.item_agi;
      newEq.atk += find.item_atk;
    });
  }

  await User.updateMany({ _id: playerId }, {
    eq_stats_def: newEq.def,
    eq_stats_vit: newEq.vit,
    eq_stats_agi: newEq.agi,
    eq_stats_atk: newEq.atk,
  });
}
