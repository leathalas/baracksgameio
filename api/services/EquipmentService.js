var EquipmentService = {
  equipItem: function (params) {
    return equipItem(params);
  },
  unequipItem: function (params) {
    return unequipItem(params);
  },
  getEquipment: function (params) {
    return getEquipment(params);
  },
};

module.exports = EquipmentService;

const Equipment = require('../models/EquipmentModel').Equipment;
const Inventory = require('../models/InventoryModel').Inventory;

const PointsService = require('../services/PointsService');

async function equipItem (params) {
  const inventory = await Inventory.findOne({ owner_id: params.playerId, item_id: params.itemId });
  const equipment = await Equipment.findOne({ owner_id: params.playerId, item_type: inventory.item_type });
  if (!inventory) {
    return { data: null, errors: "Can't find item" };
  }
  const itemCreate = {
    item_name: inventory.item_name,
    item_type: inventory.item_type,
    item_id: inventory.item_id,
    owner_id: params.playerId,
  };
  if (!equipment) {
    await Inventory.deleteOne({ owner_id: params.playerId, item_type: itemCreate.item_type });
  } else {
    await Equipment.deleteOne({ owner_id: params.playerId, item_type: itemCreate.item_type });
  }
  const item = await Equipment.create(itemCreate);
  await PointsService.updatePlayerEqStats(params.playerId);
  return { data: item, errors: "" };
}

async function unequipItem (params) {
  const equipment = await Equipment.findOne({ owner_id: params.playerId, item_type: params.itemType });
  if (!equipment) {
    return { data: null, errors: "Can't find item" };
  }
  const inventoryItem = {
    item_name: equipment.item_name,
    item_type: equipment.item_type,
    item_id: equipment.item_id,
    owner_id: params.playerId,
  };
  await Equipment.deleteOne({ owner_id: params.playerId, item_type: params.itemType });
  await Inventory.create(inventoryItem);
  await PointsService.updatePlayerEqStats(params.playerId);
  return { data: inventoryItem, errors: "" };
}

async function getEquipment (params) {
  const equipment = await Equipment.find({ owner_id: params.playerId });
  return { data: equipment, errors: "" };
}