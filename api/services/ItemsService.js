var ItemsService = {
  itemsActionFind: function (params) {
    return itemsActionFind(params);
  },
  itemActionDrop: function (params) {
    return itemActionDrop(params);
  }
};

module.exports = ItemsService;

const Item = require('../models/ItemsModel').Item;
const Mob = require('../models/MobModel').Mob;
const Inventory = require('../models/InventoryModel').Inventory;

const { drop_items } = require('../../config/itemlist');

async function itemsActionFind (params) {
  var result = null;

  switch (params.searchType) {
    case "item_name": {
      result = await Item.find({ item_name: { $regex: params.searchBody, $options: "i" }});
      break;
    }
    case "item_type": {
      result = await Item.find({ item_type: params.searchBody });
      break;
    }
    case "item_id": {
      result = await Item.findOne({ _id: params.searchBody });
      break;
    }
    case "find_all": {
      result = await Item.find();
      break;
    }
  }

  if (!result) {
    return { data: null, errors: "Error acquired while trying to access database" };
  }
  return { data: result, errors: "" };
}

async function itemActionDrop (params) {
  const mob = await Mob.findById(params.mobId)

  if (mob.mob_difficulty <= 1) {
      return { data: null, errors: "" };
  }

  let r_num = Math.ceil(Math.random() * 9);
  let r_max = r_num + (Math.ceil(r_num * 5));
  let r_min = r_num - (Math.ceil(r_num * 5));

  let d_rate = Math.ceil(Math.random() * r_max) + r_min;

  if (r_num == d_rate) {
    let items_to_drop = drop_items[mob.mob_difficulty-2].concat(drop_items[mob.mob_difficulty-1]);
    let item = await Item.findById(items_to_drop[r_num]);
    let inv_add = {
      item_name: item.item_name,
      item_type: item.item_type,
      item_id: items_to_drop[r_num],
      owner_id: params.playerId,
    };
    const added = await Inventory.create(inv_add);
    if (!added) {
        return { data: null, errors: "Error acquired while trying to add data" };
    }
    return { data: added, errors: "" };
  }

  return { data: null, errors: "" };
}
