var PotionsService = {
  usePotion: function (params) {
    return usePotion(params); 
  },
  findPotions: function (params) {
    return findPotions(params);
  },
  tryDropPotion: function (params) {
    return tryDropPotion(params);
  },
};

module.exports = PotionsService;

const Potion = require('../models/PotionsModel').Potion;
const User = require('../models/UserModel').User;

async function usePotion (params) {
  const potion = await Potion.findOne({ owner_id: params.playerId, _id: params.potionId });
  if (!potion) {
    return { data: null, errors: "Cannot find any potions" };
  }
  switch (potion.type) {
    case "health": {
      const user = await User.findById(params.playerId);
      var player_health = parseInt(user.player_health) + parseInt(potion.addition);
      if (player_health >= 100) {
          player_health = 100;
      }
      const remove = await Potion.deleteOne({ _id: params.potionId });
      const update = await User.updateOne({ _id: params.playerId }, { player_health: player_health });
      if (!remove || !update) {
          return { data: null, errors: "Something went wrong" };
      }
      return { data: update, errors: "" };
    }
  }
  return { data: null, errors: "Invalid type of potion" };
}

async function findPotions (params) {
  const potions = await Potion.find({ owner_id: params.playerId });
  if (!potions || potions.length == 0) {
      return { data: null, errors: "Cannot find any data" };
  }
  return { data: potions, errors: "" };
}

async function addPotion(params) {
  const created = await Potion.create({
    potion_name: params.potionName,
    type: params.potionType,
    addition: params.potionAddition,
    owner_id: params.playerId,
  });
  if (!created) {
    return { data: null, errors: "Error while trying to create potion" };
  }
  return { data: created, errors: "" };
}

async function tryDropPotion (params) {
  var potionName = null;
  switch (params.potionType) {
    case "health": {
      potionName = "Health potion";
      break;
    }
  }

  if (!potionName) {
    return { errors: "Wrong potion type!" };
  }

  var drop_r1 = Math.ceil(Math.random() * 10);
  var drop_r2 = Math.ceil(Math.random() * 10);
  if (drop_r1 == drop_r2) {
    params.potionName = potionName;
    for (var i = 0; i < params.potionAmount; i++) {
      const add = await addPotion (params);
      if (!add) {
        return { errors: add.errors };
      }
    }
  }
  return true;
}
