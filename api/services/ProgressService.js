var ProgressService = {
  getPlayerProgress: function (params) {
    return getPlayerProgress(params);
  },
  mobXpDrop: function (params) {
    return mobXpDrop(params);
  }
};

module.exports = ProgressService;

const User = require('../models/UserModel').User;
const mongoose = require("mongoose");

async function getPlayerProgress (params) {
  if (!mongoose.isValidObjectId(params.playerId)) {
    return { data: null, errors: "Invalid player id!" };
  }
  const user = await User.findById(params.playerId);
  if (!user) {
    return { data: null, errors: "Can not find user with that id!" };
  }
  const data = {
    player_level: user.player_level,
    player_xp: user.player_xp,
    player_lvlup_xp: calcLvlUpXp(user.player_level),
    player_curr_xp_p: calcPrc(user.player_xp, calcLvlUpXp(user.player_level)),
    player_health: user.player_health,
    player_health_p: user.player_health,
  };

  return { data: data, errors: "" }
}

async function mobXpDrop (params) {
  if (!mongoose.isValidObjectId(params.playerId)) {
    return { errors: "Invalid player id!" };
  }
  const user = await User.findById(params.playerId);
  if (!user) {
    return { errors: "Can not find user with that id!" };
  }
  const xp_drop_update = user.player_xp + (params.mobLvl + 1) * 5;
  const update = await User.updateOne({ _id: params.playerId }, { player_xp: xp_drop_update });
  const calculate = await recalculateLevel(params);
  if (!update || !calculate) {
    return { errors: "Error acquired whil trying to update data!" };
  }
  return true;
}

async function recalculateLevel (params) {
  if (!mongoose.isValidObjectId(params.playerId)) {
    return { errors: "Invalid player id!" };
  }
  const user = await User.findById(params.playerId);
  if (!user) {
    return { errors: "Can not find user with that id!" };
  }
  if (user.player_xp >= calcLvlUpXp(user.player_level)) {
    let new_xp = user.player_xp - calcLvlUpXp(user.player_level);
    let new_lvl = user.player_level + 1;
    const update = await User.updateMany({_id: params.playerId}, {
      player_xp: new_xp,
      player_level: new_lvl,
    });
    if (!update) {
      return false;
    }
  }
  return true;
}

function calcLvlUpXp (player_level) {
  return player_level * 100 + 50;
}

function calcPrc (cur, max) {
  return (cur * 100) / max;
}
