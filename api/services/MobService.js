var MobService = {
  getAllMobs: function () {
    return getAllMobs();
  },
  addNewMob: function(params) {
    return addNewMob(params);
  },
  fightMob: function(params) {
    return fightMob(params);
  },
};

module.exports = MobService;

const Mob = require("../models/MobModel").Mob;
const FightService = require("../services/FightsService");
const ItemsService = require("../services/ItemsService");
const mongoose = require("mongoose");

async function getAllMobs () {
  const data = await Mob.find();
  if (!data) {
    return { data: null, errors: "Cannot find any mobs" }
  }
  return { data: data, errors: "" };
}

async function addNewMob (params) {
  const created = await Mob.create(params);
  if (!created) {
    return { data: null, errors: "Error acquired while trying to add data" };
  }
  console.log("Created Mob");
  return { data: created, errors: "" };
}

async function fightMob (params) {
  if (!mongoose.isValidObjectId(params.playerId) || !mongoose.isValidObjectId(params.mobId)) {
    return { data: null, errors: "Invalid data format" };
  }
  const fight = await FightService.handleMobFight(params);

  if (fight.hasOwnProperty("errors")) {
    return { data: null, errors: fight.errors };
  }

  if (fight.isPlayerWin) {
    const drop = await ItemsService.itemActionDrop(params);
    return { data: fight, drop: drop.data, errors: "" };
  }

  return { data: fight, errors: "" };
}
