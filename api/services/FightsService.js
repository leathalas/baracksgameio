var FightsService = {
  handleMobFight: function(params) {
    return handleMobFight(params);
  },
};

module.exports = FightsService;

const User = require("../models/UserModel").User;
const Mob = require("../models/MobModel").Mob;

const ProgressService = require("../services/ProgressService");
const PotionsService = require('../services/PotionsService');

async function handleMobFight (params) {
  const mobs = await Mob.findById(params.mobId);
  const user = await User.findById(params.playerId);
  
  if (!mobs || !user) {
    return { errors: "Error aquired while trying to fetch data from database" };
  }

  let m_Stats = {
    def: mobs.mb_stats_def,
    agi: mobs.mb_stats_agi,
    vit: mobs.mb_stats_vit,
    atk: mobs.mb_stats_atk
  };

  let p_Stats = {
    def: user.pt_stats_def + user.eq_stats_def,
    agi: user.pt_stats_agi + user.eq_stats_agi,
    vit: user.pt_stats_vit + user.eq_stats_vit,
    atk: user.pt_stats_atk + user.eq_stats_atk
  };

  let m_Attack = m_Stats.atk + ((m_Stats.atk * m_Stats.agi) / 100);
  let p_Attack = p_Stats.atk + ((p_Stats.atk * p_Stats.agi) / 100);

  m_Attack = m_Attack / ((50 * p_Stats.def) / 100);
  p_Attack = p_Attack / ((50 * m_Stats.def) / 100);

  let m_Hits = p_Stats.vit / m_Attack;
  let p_Hits = m_Stats.vit / p_Attack;

  await dealDamage(params.playerId, p_Hits - (m_Hits * m_Attack / 2));

  params.mobLvl = mobs.mob_difficulty;
  
  if (m_Hits < p_Hits) {
    return { isPlayerWin: false };
  } else {
    const data = await ProgressService.mobXpDrop(params);

    let potion_data = {
      playerId: params.playerId,
      potionType: "health",
      potionAmount: mobs.mob_difficulty + 1,
      potionAddition: 5 * (mobs.mob_difficulty + 1),
    };

    await PotionsService.tryDropPotion(potion_data);

    if (data.errors) {
      return { isPlayerWin: true, errors: data.errors };
    }
    return { isPlayerWin: true };
  }
}

async function dealDamage(playerId, amount) {
  console.log(amount);
  if (amount <= 0) {
    return false;
  }
  const user = await User.findById(playerId);
  let newHp = user.player_health - Math.ceil(amount);
  if (newHp <= 0) {
    newHp = 0;
  }
  const update = await User.updateOne({ _id: playerId }, { player_health: newHp });
  if (!update) {
    return false;
  }
  return true;
}