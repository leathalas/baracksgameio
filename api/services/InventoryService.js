var InventoryService = {
  getInventory: function(params) {
    return getInventory(params);
  },
  addItem: function(params) {
    return addItem(params);
  },
};

module.exports = InventoryService;

const Inventory = require('../models/InventoryModel').Inventory;

async function getInventory (params) {
  const inventory = await Inventory.find({ owner_id: params.playerId });
  if (!inventory) {
    return { data: null, errors: "Error acquired while trying to get data" };
  }
  return { data: inventory, errors: "" };
}

async function addItem (params) {
  const created = await Inventory.create(params);
  if (!created) {
    return { data: null, errors: "Error acquired while trying to add data" };
  }
  console.log("Created item");
  return { data: created, errors: "" };
}