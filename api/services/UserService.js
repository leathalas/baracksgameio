var UserService = {
  createUser: function (params) {
    return createUser(params);
  },
  connectUser: function (params) {
    return connectUser(params);
  },
};

module.exports = UserService;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { secretKey } = require('../../config/globals');
const User = require('../models/UserModel').User;

async function createUser(params) {
  let hashed = await bcrypt.hash(params.password, 15);
  const userCreate = {
    email: params.email,
    username: params.username,
    password: hashed,
  };
  let exists = await User.findOne({ username: params.username });
  if (exists) {
    return { data: null, errors: "User with that username exists" };
  }
  const user = await User.create(userCreate);
  return { data: user, errors: "" };
}

async function connectUser(params) {
  if (!params.username || !params.password) {
    return { errors: "Username or password is incorrect" };
  }
  let user = await User.findOne({ username: params.username });
  if (!user) {
    return { errors: "Username or password is incorrect" };
  }
  let login = await bcrypt.compare(params.password, user.password);
  if (!login) {
    return { errors: "Username or password is incorrect" };
  }
  const token = await jwt.sign({ user }, secretKey);
  if (!token) {
    return { errors: "Username or password is incorrect" };
  }
  return { token: token, errors: "" };
}
