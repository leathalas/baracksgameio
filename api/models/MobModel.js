const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MobSchema = new Schema({
    mob_name: {type: String, required: true},
    mob_difficulty: {type: Number, required: true},
    mb_stats_def: {type: Number, default: 0},
    mb_stats_agi: {type: Number, default: 0},
    mb_stats_vit: {type: Number, default: 0},
    mb_stats_atk: {type: Number, default: 0},
});

module.exports = {Mob: mongoose.model('Mobs', MobSchema)};