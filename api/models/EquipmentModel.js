const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EquipmentSchema = new Schema({
    item_name: {type: String},
    item_type: {type: String},
    item_id: {type: String},
    owner_id: {type: String}
});

module.exports = {Equipment: mongoose.model('Equipment', EquipmentSchema)};