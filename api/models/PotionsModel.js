const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PotionsSchema = new Schema({
    potion_name: {type: String, require: true},
    type: {type: String, require: true},
    addition: {type: Number, require: true},
    owner_id: {type: String, require: true}
});

module.exports = {Potion: mongoose.model('Potions', PotionsSchema)};