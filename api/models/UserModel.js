const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: { type: String, unique: true },
  email: { type: String },
  password: { type: String },
  pt_stats_def: { type: Number, default: 0 },
  pt_stats_agi: { type: Number, default: 0 },
  pt_stats_vit: { type: Number, default: 0 },
  pt_stats_atk: { type: Number, default: 0 },
  eq_stats_def: { type: Number, default: 0 },
  eq_stats_agi: { type: Number, default: 0 },
  eq_stats_vit: { type: Number, default: 0 },
  eq_stats_atk: { type: Number, default: 0 },
  player_health: { type: Number, default: 100 },
  player_points: { type: Number, default: 10 },
  player_level: { type: Number, default: 0 },
  player_xp: { type: Number, default: 0 },
});

module.exports = { User: mongoose.model('Users', UserSchema) };