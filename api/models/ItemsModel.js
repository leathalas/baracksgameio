const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
  item_name: { type: String, unique: true },
  item_type: { type: String },
  item_level: { type: Number },
  item_def: { type: Number },
  item_atk: { type: Number },
  item_agi: { type: Number },
  item_vit: { type: Number }
});

module.exports = { Item: mongoose.model('Item', ItemSchema) };