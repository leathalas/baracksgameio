const express = require('express');
const router = express.Router();
//const verifyToken = require('../api/controllers/authentication/VerifyUserToken');
const validateUserRegister = require('../api/controllers/authentication/ValidateUserRegister');

const UserController = require('../api/controllers/UserController');
const ItemController = require('../api/controllers/ItemsController');
const PointsController = require('../api/controllers/PointsController');
const InventoryController = require('../api/controllers/InventoryController');
const EquipmentController = require('../api/controllers/EquipmentController');
const MobController = require('../api/controllers/MobController');
const ProgressController = require('../api/controllers/ProgressController');
const PotionsController = require('../api/controllers/PotionsController');

router.get('/api/item/action/find/:searchtype/:searchbody', ItemController.itemsActionFind);

router.get('/api/player/inventory/:playerid', InventoryController.getPlayerInventory);
router.get('/api/player/equipment/:playerid', EquipmentController.getPlayerEquipment);

router.get('/api/add/item', InventoryController.additemToInventory);
router.get('/api/add/mob', MobController.createMob);

router.get('/api/mobarena/mobs/all', MobController.getAllMobs);
router.post('/api/mobarena/fight/mob', MobController.fightMob);

router.get('/api/player/info/points/:playerid', PointsController.getPlayerPointsInfo);
router.get('/api/player/info/base/:playerid', ProgressController.getPlayerProgress);
router.post('/api/player/add/player/stats/', PointsController.addPlayerStats);

router.post('/api/equipment/equipitem/', EquipmentController.equipItemById);
router.post('/api/equipment/unequipitem/', EquipmentController.unequipItemById);

router.post('/api/login/', UserController.login);
router.post('/api/register/', validateUserRegister.validate('UserRegister'), UserController.register);

router.post('/api/player/potions/use/', PotionsController.usePotion);
router.get('/api/player/potions/all/:playerid', PotionsController.potionsStorage);

module.exports = router;